use athene::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct QueryParam<'a> {
    pub label: &'a str,
    pub keyword: &'a str,
}

#[derive(Default, Serialize, Deserialize)]
pub struct BasicController {
    pub label: String,
    pub keyword: String,
}

impl Controller for BasicController {
    const BASE_PATH: &'static str = "/api/v1/basic";

    fn method(&self) -> Vec<ControllerMethod<Self>>
    where
        Self: Sized,
    {
        ControllerBuilder::new()
            .post("/add", Self::add)
            .delete("/{label}/{keyword}", Self::delete)
            .put("/update", Self::update)
            .get("/get", Self::query_get)
            .build()
    }
}

impl BasicController {

    // http://127.0.0.1:7878/api/v1/basic/add
    // Context-Type : application/json  Or application/x-www-form-urlencoded
    pub async fn add(&self, mut req: Request) -> impl Responder {
        let obj = req.parse::<Self>().await?;
        Ok::<_, Error>((200, Json(obj)))
    }

    // http://127.0.0.1:7878/api/v1/basic/
    pub async fn delete(&self, mut req: Request) -> impl Responder {
        let lable = req.param::<String>("label")?;
        let keyword = req.param::<String>("keyword")?;

        Ok::<_, Error>((200, format!("lable = {},keyword = {}", lable, keyword)))
    }

    // http://127.0.0.1:7878/api/v1/basic/update
    // Context-Type : application/json  Or application/x-www-form-urlencoded
    pub async fn update(&self, mut req: Request) -> impl Responder {
        let obj = req.parse::<Self>().await?;
        Ok::<_, Error>((200, Json(obj)))
    }

    // http://127.0.0.1:7878/api/v1/basic/get
    pub async fn query_get(&self, req: Request) -> impl Responder {
        let arg = req.query::<QueryParam>()?;
        let res = Builder::new();

        Ok::<_, Error>(res.status(StatusCode::OK).json(&arg))
    }
}

#[tokio::main]
pub async fn main() -> Result<(), Error> {
    let app = athene::new()
        .router(|r| r.controller(BasicController::default()));
    app.listen("127.0.0.1:7878").await
}
