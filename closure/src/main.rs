use athene::prelude::*;
use serde::{Serialize,Deserialize};

#[derive(Serialize,Deserialize)]
pub struct QueryParams {
    pub name: String,
    pub age: u16,
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new().router(|r| {
        
        r.get("/index", |_| async {
            "index"
        })

        .get("/remote_addr", |req: Request|async move {
            let remote_addr = req.remote_addr().map(|addr|addr.to_string());
            (200,remote_addr)
        })

        // Uri Params
        .delete("/{name}", |mut req: Request| async move {
            let name = req.param::<String>("name")?;

            Ok::<_,Error>((StatusCode::OK,name))
        })

        // Query Params
        .get("/query/",|req: Request| async move {
            let query_params = req.query::<QueryParams>()?;
            Ok::<_,Error>((StatusCode::OK,Json(query_params)))
        })

        // Permanent Redirect
        .get("/permanent",|_| async {
            let res = Builder::new();
            res.redirect(StatusCode::PERMANENT_REDIRECT,"/remote_addr")
        })

        // Temporary Redirect
        .get("/temporary",|_| async {
            let res = Builder::new();
            res.redirect(StatusCode::TEMPORARY_REDIRECT,"/index")
        })
    });
    app.listen("127.0.0.1:7878").await
}
