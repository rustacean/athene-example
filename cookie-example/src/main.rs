use athene::prelude::*;

struct Cookies;

// http://127.0.0.1:7878/cookies
#[controller]
impl Cookies {

    // http://127.0.0.1:7878/api/v1/user/get_cookies
    #[get("/get_cookies")]
    pub async fn get_cookies(&self,cookie_jar: CookieJar) -> impl Responder {
        let cookie = cookie_jar.get("Cookie1").unwrap();
        println!("cookie = {:?}", cookie);
    }

    // http://127.0.0.1:7878/api/v1/user/set_cookies
    #[get("/set_cookies")]
    pub async fn set_cookies(&self) -> impl Responder {
        let mut cookie = Cookie::new("Cookie1", "athene");
        cookie.set_path("/");
        let mut cookie2 = Cookie::new("Cookie2", "athene2");
        cookie2.set_path("/");
        let mut cookie_jar = CookieJar::new();
        cookie_jar.add(cookie);
        cookie_jar.add(cookie2);
        (200, cookie_jar)
    }
}

pub async fn get_cookies(mut req: Request) -> Response {
    let cookie = req.cookie("test1").unwrap();
    let mut res = Response::default();
    *res.status_mut() = StatusCode::OK;
    res.text(format!("cookie_name = {:?}",cookie.name()));
    res
}
pub async fn set_cookies(_req: Request) -> impl Responder {
    let mut cookie = Cookie::new("test1", "athene");
    cookie.set_path("/");

    let mut cookie2 = Cookie::new("test2", "athene2");
    cookie2.set_path("/");

    let mut cookie_jar = CookieJar::new();

    cookie_jar.add(cookie);
    cookie_jar.add(cookie2);

    (200, cookie_jar)
}

fn cookie_router(r: Router) -> Router {
    r.get("/set_cookies", set_cookies)
     .get("/get_cookies", get_cookies)
}

#[tokio::main]
async fn main() -> Result<()> {
    let app = athene::new();

    let app = app.router(cookie_router).router(|r|r.controller(Cookies));
    
    app.listen("127.0.0.1:7878").await
}
