use athene::prelude::*;

#[derive(Debug)]
pub enum CustomError {
    NotFound(String),
    UserNotExist(u16,String),
}

impl Responder for CustomError {
    fn response(self, builder: Builder) -> Builder {
        /*
            do something
         */
        match self {
            Self::NotFound(s) => builder.status(404).text(s.to_string()),
            Self::UserNotExist(d,s) => builder.status(d).text(s.to_string())
        }
    }
}

// http://127.0.0.1:7878/error
#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new()
        .router(|r|
            r.get("/error", |_req: Request| async {
                CustomError::NotFound(String::from("NotFound"))
            })
        );
        
    app.listen("127.0.0.1:7878").await
}