use athene::prelude::*;

pub async fn download(_req: Request) -> impl Responder {
    let mut res = Response::default();
    res
        .write_file("src/lib.rs", DispositionType::Attachment)
        .unwrap();
    res
}

// 127.0.0.1:7878/download
#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new();
    let app = app.router(|r| r.get("/download", download));
    app.listen("127.0.0.1:7878").await
}
