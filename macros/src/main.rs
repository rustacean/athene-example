use athene::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct UserController {
    pub email: String,
    pub age: u16,
}

// http://127.0.0.1:7878/api/v1/user
#[controller(prefix = "api", version = 1, name = "user")]
impl UserController {
    // http://127.0.0.1:7878/api/v1/user/create
    // Context-Type : application/json
    #[post("/create")]
    async fn create(&self, user: Json<Self>) -> impl Responder {
        Ok::<_, Error>((200, user))
    }

    // http://127.0.0.1:7878/api/v1/user/admin/18
    #[delete("/{username}/{age}")]
    pub async fn delete(&self, username: String, age: Option<u16>) -> impl Responder {
        (
            StatusCode::OK,
            format!("username is : {}, and age is : {:?}", username, age),
        )
    }

    // http://127.0.0.1:7878/api/v1/user/update
    // Context-Type : application/x-www-form-urlencoded
    #[put("/update")]
    #[post("/parse_form_body")]
    #[get("/parse_form_body")]
    async fn update(&self, user: Form<Self>) -> impl Responder {
        Ok::<_, Error>((200, user))
    }

    // http://127.0.0.1:7878/api/v1/user/query_get/?username=admin&age=29
    #[get("/query_get")]
    pub async fn query_get(&self, email: String, age: u16) -> impl Responder {
        (200, Json(Self { email, age }))
    }

    // http://127.0.0.1:7878/api/v1/user/query/?username=admin@qq&age=19
    #[get("/query")]
    pub async fn query(&self, user: Query<Self>) -> impl Responder {
        (200, Json(user.0))
    }

    // http://127.0.0.1:7878/api/v1/user/parse_body_vec
    #[post("/parse_body_vec")]
    #[get("/parse_body_vec")]
    pub async fn parse_body_vec(&self, mut req: Request) -> impl Responder {
        let vector = req.parse_body::<Vec<u8>>().await?;
        Ok::<_, Error>((StatusCode::OK, vector))
    }

    // http://127.0.0.1:7878/api/v1/user/parse_body_string
    #[post("/parse_body_string")]
    #[get("/parse_body_string")]
    pub async fn parse_body_string(&self, mut req: Request) -> impl Responder {
        let vector = req.parse_body::<String>().await?;
        Ok::<_, Error>((StatusCode::OK, vector))
    }
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new()
        .router(|r| r.controller(UserController::default()))
        .middleware(|r| r);

    app.listen("127.0.0.1:7878").await
}
