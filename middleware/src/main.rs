use athene::prelude::*;
use headers::{authorization::Bearer, Authorization};
use serde::{Deserialize, Serialize};
use tracing::info;

#[derive(Serialize, Deserialize)]
pub struct User {
    pub username: String,
    pub age: u16,
}

// 127.0.0.1:7878/user/admin/18
pub async fn user_params(mut req: Request) -> impl Responder {
    let username = req.param("username")?;
    let age = req.param::<u16>("age")?;
    Ok::<_, Error>((200, Json(User { username, age })))
}

// 127.0.0.1:7878/user/sign_up
pub async fn sign_up(mut req: Request) -> impl Responder {
    let user = req.parse::<User>().await?;
    Ok::<_, Error>((200, Json(user)))
}

// 127.0.0.1:7878/user/body_to_vec_u8
pub async fn body_to_vec_u8(mut req: Request) -> impl Responder {
    let body = req.parse_body::<Vec<u8>>().await?;
    Ok::<_, Error>((200, body))
}

// 127.0.0.1:7878/user/body_to_string
pub async fn body_to_string(mut req: Request) -> impl Responder {
    let body = req.parse_body::<String>().await?;
    Ok::<_, Error>((200, body))
}

pub fn user_router(r: Router) -> Router {
    r.group("/user")
        .get("/{username}/{age}", user_params)
        .post("/login", login)
        .post("/sign_up", sign_up)
        .put("/body_to_vec_u8", body_to_vec_u8)
        .post("/body_to_string", body_to_string)
}

// 127.0.0.1:7878/user/login
pub async fn login(mut req: Request) -> impl Responder {
    if let Ok(user) = req.parse::<User>().await {
        (
            201,
            format!("username: {} , age: {} ", user.username, user.age),
        )
    } else {
        (400, String::from("Bad user format"))
    }
}

pub async fn log_middleware(ctx: Context, next: &dyn Next) -> Result {
    let uri_path = ctx.state.request().map(|req| req.uri().path());
    info!("new request on path: {:?}", uri_path);
    let ctx = next.next(ctx).await?;
    let status = ctx.state.response().map(|res| res.status());
    info!("new response with status: {:?}", status);
    Ok(ctx)
}

struct ApiKeyMiddleware {
    api_key: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct MiddlewareError<T> {
    pub code: u32,
    pub msg: String,
    pub data: T
}

#[middleware]
impl ApiKeyMiddleware {
    async fn next(&self, ctx: Context, chain: &dyn Next) -> Result {
        if let Some(req) = ctx.state.request() {
            if let Some(bearer) = req.header::<Authorization<Bearer>>() {
                let token = bearer.0.token();
                if token == self.api_key {
                    info!(
                        "Handler {} will be used",
                        ctx.metadata.name.unwrap_or("unknown")
                    );
                    chain.next(ctx).await
                } else {
                    info!("Invalid token");
                    let res = MiddlewareError {
                        code: 400,
                        msg: String::from("Invalid token"),
                        data: "Invalid token"
                    };
                    Err(Error::Json(json!(res)))
                }
            } else {
                info!("Not Authenticated");
                Err(Error::Response(404,json!("Not Authenticated")))
            }
        } else {
            Ok(ctx)
        }
    }
}

#[tokio::main]
pub async fn main() -> Result<()> {

    tracing_subscriber::fmt().compact().init();

    let app = athene::new().router(user_router).middleware(|m| {
        m.apply(log_middleware, vec!["/"], None)
         .apply(
            ApiKeyMiddleware {
                api_key: "athene".to_string(),
            },
            vec!["/user/login", "/user/sign_up"], 
            vec!["/user/body_to_vec_u8", "/user/body_to_string"], 
        )
    });

    app.listen("127.0.0.1:7878").await
}
