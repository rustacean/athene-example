use athene::prelude::*;

#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new().router(|r| {
        let r = r.get("/**", StaticDir::new("static-dir").with_listing(true));
        r.get("/hello", |_|async{
            "hello world"
        })
    });
    app.listen("127.0.0.1:7878").await
}
