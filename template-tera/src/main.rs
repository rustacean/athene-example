use athene::prelude::*;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use tera::{Context as TeraContext, Tera};

static TERA: Lazy<Tera> = Lazy::new(|| Tera::new("template-tera/templates/*").unwrap());

#[derive(Debug, Serialize, Clone)]
pub struct Flash {
    pub kind: String,
    pub message: String,
}

#[derive(Deserialize, Debug)]
pub struct CreateForm {
    description: String,
}

#[derive(Deserialize, Debug)]
pub struct UpdateForm {
    _method: String,
}

#[derive(Debug, Serialize, Clone,Default)]
pub struct Task {
    pub id: i32,
    pub description: String,
    pub completed: bool,
}

// http://127.0.0.1:7878/task
#[controller]
impl Task {

    // http://127.0.0.1:7878/task/index
    #[get("/index")]
    pub async fn index(&self,_req: Request) -> impl Responder {
        let tasks = [
            Task {
                id: 1,
                description: "Buy a coffee ".to_string(),
                completed: true,
            },
            Task {
                id: 2,
                description: "Shopping".to_string(),
                completed: true,
            },
            Task {
                id: 3,
                description: "Coding".to_string(),
                completed: false,
            },
        ]
        .to_vec();

        let flash = Flash {
            kind: "Dangerous".to_string(),
            message: "Danger, stay away".to_string(),
        };

        let mut ctx = TeraContext::new();
        ctx.insert("tasks", &tasks);
        ctx.insert("msg", &(flash.kind, flash.message));

        let html = TERA.render("index.html", &ctx).unwrap();
        Html(html)
    }

    #[post("/todo")]
    pub async fn create(&self,form: Form<CreateForm>) -> impl Responder {
        println!("{:?}",form.0);
        StatusCode::CREATED
    }

    #[post("/todo/{id}")]
    pub async fn update(&self,id: u16, form: Form<UpdateForm>) -> impl Responder {
        println!("id = {}",id);
        println!("{:?}",form.0);
        StatusCode::CREATED
    }
}

// http://127.0.0.1:7878/task/index
#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new();
    let app = app.
    router(|r|r.static_dir("/**", "template-tera/static").controller(Task::default()));
    app.listen("127.0.0.1:7878").await
}
