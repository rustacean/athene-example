use athene::prelude::*;

static INDEX_HTML: &str = r#"<!DOCTYPE html>
<html>
    <head>
        <title>Upload file</title>
    </head>
    <body>
        <h1>Upload file</h1>
        <form action="/upload" method="post" enctype="multipart/form-data">
            <input type="file" name="file" />
            <input type="submit" value="upload" />
        </form>
    </body>
</html>
"#;

// http://127.0.0.1:7878/upload
pub async fn upload(mut req: Request) -> impl Responder {
    let res = req.upload("file", "temp").await?;
    if res > 0 {
        Ok::<_, Error>((200, "File uploaded successfully"))
    } else {
        Ok::<_, Error>((400, "File upload failed"))
    }
}

pub fn file_router(r: Router) -> Router {
    let r = r
        .get("/upload", |_: Request| async { Html(INDEX_HTML) })
        .post("/upload", upload);
    r
}

#[tokio::main]
pub async fn main() -> Result<()> {

    let app = athene::new().router(file_router);

    app.listen("127.0.0.1:7878").await
}
