use athene::prelude::*;

static INDEX_HTML: &str = r#"<!DOCTYPE html>
<html>
    <head>
        <title>Upload files</title>
    </head>
    <body>
        <h1>Upload files</h1>
        <form action="/uploads" method="post" enctype="multipart/form-data">
            <input type="file" name="files" />
            <input type="submit" value="upload" />
        </form>
    </body>
</html>
"#;

// http://127.0.0.1:7878/uploads
pub async fn uploads(mut req: Request) -> impl Responder {
    let msg = req.uploads("files", "temp").await?;
    Ok::<_, Error>((200, msg))
}

pub fn file_router(r: Router) -> Router {
    let r = r
        .get("/upload", |_: Request| async { Html(INDEX_HTML) })
        .post("/uploads", uploads);
    r
}

#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new().router(file_router);

    app.listen("127.0.0.1:7878").await
}
