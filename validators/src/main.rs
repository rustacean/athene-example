use athene::prelude::*;
use serde::{Deserialize, Serialize};
use validator::Validate;

#[derive(Debug, Serialize, Deserialize, Validate, Default)]
pub struct UserController {
    #[validate(email)]
    pub email: String, // admin@outlook.com
    #[validate(range(min = 18, max = 20))]
    pub age: u16,
}

// http://127.0.0.1:7878/user
#[controller(prefix = "api", version = 1, name = "user")]
impl UserController {
    // http://127.0.0.1:7878/api/v1/user/create
    // Context-Type : application/json
    #[post("/create")]
    async fn create(&self, user: Json<Self>) -> impl Responder {
        Ok::<_, Error>((200, user))
    }

    // http://127.0.0.1:7878/api/v1/user/admin/18
    #[delete("/{email}/{age}")]
    pub async fn delete(&self, email: String, age: Option<u16>) -> impl Responder {
        (
            StatusCode::OK,
            format!("email is : {}, and age is : {:?}", email, age),
        )
    }

    // http://127.0.0.1:7878/api/v1/user/update
    // Context-Type : application/x-www-form-urlencoded
    #[put("/update")]
    #[validator(exclude("user"))] // user will not be verified
    async fn update(&self, user: Form<Self>) -> impl Responder {
        Ok::<_, Error>((200, user))
    }

    // http://127.0.0.1:7878/api/v1/user/query_get/?username=admin&age=29
    #[get("/query_get")]
    pub async fn query_get(&self, email: String, age: u16) -> impl Responder {
        (200, Json(Self { email, age }))
    }

    // http://127.0.0.1:7878/api/v1/user/query/?username=admin@qq&age=19
    #[get("/query")]
    pub async fn query(&self, user: Query<Self>) -> impl Responder {
        (200, Json(user.0))
    }
}

#[tokio::main]
pub async fn main() -> Result<()> {
    tracing_subscriber::fmt().compact().init();

    let app = athene::new();
    let app = app.router(|r| r.controller(UserController::default()));
    app.listen("127.0.0.1:7878").await
}
