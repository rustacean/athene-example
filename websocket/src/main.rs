use std::sync::Arc;

use std::collections::HashMap;
use athene::prelude::*;
use tokio::sync::{mpsc::{self,Sender,Receiver},RwLock};

// v1 websocket handler
pub async fn websocket(
    _req: Request,
    tx: WebSocketSender,
    rx: WebSocketReceiver,
) -> Result<()> {
    // Message Channel 
    let (sender_tx,sender_rx) = mpsc::channel::<Message>(1000);
    tokio::spawn(write(tx,sender_rx));
    tokio::spawn(read(rx,sender_tx));
    Ok(())
}

async fn read(mut receiver: WebSocketReceiver,sender_tx: Sender<Message>){
    while let Ok(Some(msg)) = receiver.receive().await {
        match msg {
            Message::Text(text) => {
                println!("{text}");
                sender_tx.send(Message::Text(text)).await.unwrap();
            }
            Message::Close(_) => break,
            _=> {}
        }
    }
}

async fn write(mut sender: WebSocketSender,mut sender_rx: Receiver<Message>) {
    while let Some(msg) = sender_rx.recv().await {
        sender.send(msg).await.unwrap();
    }
}

static USER_ID: u16 = 1;

#[derive(Default)]
pub struct AppState {
    pub clients: Arc<RwLock<HashMap<u16,WebSocketSender>>>,
}
// v2 
pub async fn websocket02(
    _req: Request,
    tx: WebSocketSender,
    rx: WebSocketReceiver,
) -> Result<()> {

    let app_state = AppState::default();
    app_state.clients.write().await.insert(USER_ID,tx);

    tokio::spawn(read_app_state(rx,app_state));
    Ok(())
}

async fn read_app_state(mut receiver: WebSocketReceiver,app_state: AppState){
    while let Ok(Some(msg)) = receiver.receive().await {
        match msg {
            Message::Text(text) => {
                println!("{text}");
            }
            Message::Close(_) => break,
            _=> {}
        }
    }
    // remove client 
    app_state.clients.write().await.remove(&USER_ID);
}

// async fn write(mut sender: WebSocketSender,mut sender_rx: Receiver<Message>) {
//     while let Some(msg) = sender_rx.recv().await {
//         sender.send(msg).await.unwrap();
//     }
// }

// ws://127.0.0.1:7878/ws  http://www.jsons.cn/websocket/
#[tokio::main]
async fn main() -> Result<()> {

    // let (sender_tx,sender_rx) = mpsc::channel::<Message>(1000);

    // let app_state = AppState {
    //     clients: Arc::new(data)
    // }


    let app = athene::new().router(|r| {
        
        // ws://127.0.0.1:7878/ws
        let r = r.ws("/ws", |_req, mut tx, mut rx| async move {
            while let Some(msg) = rx.receive().await? {
                tx.send(msg).await?;
            }
            Ok(())
        });
        // ws://127.0.0.1:7878/ws/websocket
        let r = r.ws("/ws/websocket", websocket);
        r
    });

    app.listen("127.0.0.1:7878").await

    // let mut sender_task = tokio::spawn(async move {

    // });

    // server_task
    // let mut server_task = tokio::spawn(async move {
    //     app.listen("127.0.0.1:7878").await
    // });

    // tokio::select! {
    //     _ = (&mut sender_task) => {
    //         println!("Sender Exited, Stopping");
    //         server_task.abort();
    //     }

    //     _ = (&mut server_task) => {
    //         println!("Server Exited, Stopping");
    //         sender_task.abort();
    //     }

    //     _ = tokio::signal::ctrl_c() => {
    //         println!("CTRL + C Received, Stopping");
    //         sender_task.abort();
    //         server_task.abort();
    //     }
    // }
    // Ok(())
}
